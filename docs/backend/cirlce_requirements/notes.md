# Note TBD

## Overview

Circle should have a notes that are shared between users in the circle.

## Requirements

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD notes (each action having its own permission)

#### Functionality

-   User can create a note.
-   User can update an existing note.
-   User can view/read a note.
-   User can list/view all available notes.
-   User can delete a note.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on notes.

#### Permissions list

- **Create Note**: User can create a note.
- **Update Note**: User can update an existing note.
- **View Any Note**: User can view any (single) existing note.
- **View All Note**: User can view all/list existing notes.
- **Delete Note**: User can delete an existing notes.

## Optional functionalities

-   **WYSIWYG Support**

Support WYSIWYG special characters to allow users to use WYSIWYG text editors

-   **Markdown Support**

Support markdown characters to allow for simple text formatting.

-   **Imbedded Pictures**

Support embedding pictures in notes.

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)
-   [Glossary](../../extras/glossary.md)
