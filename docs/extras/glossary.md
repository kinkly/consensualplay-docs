# Glossary

## C

-   Cron: An expression of a schedule. [Examples](https://www.baeldung.com/cron-expressions),[Cron generator](https://crontab.cronhub.io/), [Wikipedia](https://en.wikipedia.org/wiki/Cron)
-   CRUD: Create, Read, Update, Delete

## M

-   Markdown: A lightweight markup language for creating formatted text using a plain-text editor. [Wikipedia](https://en.wikipedia.org/wiki/Markdown)

## T

- TBD: Acronym meaning "to be developed", most likely meaning page still under development.

## W

-   WYSIWYG: An acronym for "what you see is what you get". [Wikipedia](https://en.wikipedia.org/wiki/WYSIWYG)
