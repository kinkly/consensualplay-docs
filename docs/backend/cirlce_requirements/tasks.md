# Task TBD

## Overview

Tasks can be assigned to users by other users or self, tasks can have many form.

## Requirements

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Points](points.md)
-   [Consequences](consequences.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **Trigger Consequences/points**

#### Goal

Tasks should effect users points or assign consequences to them.

#### Functionality

-   Task can increase users points a pone completion
-   Task can decrease users points a pone missing completion target
-   Task can assign users consequences a pone completion or fail

### 2. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD tasks (each action having its own permission)

#### Functionality

-   User can create/assign a task to a user.
-   User can update/pause/resume an existing task.
-   User can view/read a tasks.
-   User can list/view all available tasks.
-   User can delete a task.

### 3. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on tasks.

#### Permissions list

-   **Create/Assign Task to all**: User can create/assign tasks to all users.
-   **Create/Assign Task to any**: User can create/assign tasks to any (single) user.
-   **Update Task**: User can update an existing task.
-   **View Any Task**: User can view any (single) existing task.
-   **View All Task**: User can view all/list existing tasks.
-   **Delete Task**: User can delete an existing tasks.

### 4. **Task types**

#### Goal

Task should be able have different types

#### types

-   **One time**

A one time task is only needed to be completed once, it can have unlimited or limited time limit.

-   **Unlimited**

An unlimited task can be completed indefinitely, a schedule should be set to control how often the task should be completed. The **trigger Consequences/points** should be triggered a pone each completion.

_Recommendation use CRON schedule format_: [Wiki](https://en.wikipedia.org/wiki/Cron)

Example: An unlimited task with a cron schedule of "0 0 0 \* \*" (one a week)

-   **Limited**

A limit task can be done until the limit i hit, the limit can be a completion count or date limit or both, a schedule should be set to control how often the task should be completed. The **trigger Consequences/points** should be triggered a pone each completion or overall success or fail at the end of the limit.

Time limited Ex: Task should be completed however many times until 7 days from now, Schedule is twice a day (midnight and midday).

Completion count limited Ex: Task should be completed 14 times, Schedule is twice a day (midnight and midday).

-   **Unlimited periodic cycle**

A periodic cycle task can be done a number of times per cycle before a completion counter resets, the cycle can be set by time limits or completion count limits. The **trigger Consequences/points** should be triggered a pone each completion or fail of a whole cycle.

Examples:

Time cycle: Task should be done according to schedule within a weekly time limit. Schedule is once a day, each week the weekly time limit resets

Completion cycle: Task should be done according to schedule for a limit completion limit. Schedule is once a day, each week the 7 completions limit resets (each week user must complete the task 7 times before the counter resets)

-   **Limited periodic cycle**

Same as **unlimited periodic cycle** but is limited by date or cycle count, The **trigger Consequences/points** should be triggered a pone each completion or fail of a whole cycle.

## Optional functionalities

-   **WYSIWYG Support**

Support WYSIWYG special characters to allow users to use WYSIWYG text editors

-   **Markdown Support**

Support markdown characters to allow for simple text formatting.

-   **Imbedded Pictures**

Support embedding pictures in tasks.

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Points](points.md)
-   [Consequences](consequences.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)
-   [Glossary](../../extras/glossary.md)
