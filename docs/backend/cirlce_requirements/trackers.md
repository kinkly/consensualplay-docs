# Tracker TBD

## Overview

In a circle a tracker is basically an attribute that has a value, the change of that value is stored with the date of change, the idea is to be able to track progress of some sort, a tracker should be manually modifiable, auto modification can also be an option although i don't see good way to it.

In this document i will focus on manual implementation.

## Minimum Elements

    - target user: the user that this tracker relates to
    - title: title of tracker
    - value: current value of tracker
    - update_date: date of when this value is set

## Example

User add a Push up tracker:

    - title: Max push up count by user
    - value: 5
    - update_date: 01.01.2024

After a month user updates tracker: - value: 20 - update_date: 01.02.2024

User can see that this tracker has one historical value and also see the current one.

## Requirements

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD tracker (each action having its own permission)

#### Functionality

-   User can create a tracker.
-   User can update an existing tracker.
-   User can view/read a tracker.
-   User can list/view all available trackers.
-   User can delete a tracker.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on trackers.

#### Permissions list

-   **Create tracker**: User can create a tracker.
-   **Update tracker**: User can update an existing tracker.
-   **View Any tracker**: User can view any (single) existing tracker.
-   **View All trackers**: User can view all/list existing trackers.
-   **Delete tracker**: User can delete an existing tracker.

### 3. **Historical view**

#### Goal

User can see all the past values of the tracker along with the dates of change

#### Functionality

-   User can list all the past values for tracker
-   User can filter historical information by value and/or change date

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Glossary](../../extras/glossary.md)
