# Ideas

This list is all the ideas that were gathered from suggestions of the community, they are listed in as a very rough minimal information.

## List

### App basics

-   multi device (users can use the app on any number of devices)
-   peer 2 peer preferably (encrypted)
-   password protected
-   backup functionality
-   encrypted
-   as many users as possible
-   themes
-   custom start of day
    -   set custom time to start new day (example 2am)
-   password protection
-   change icon (hide app in plain sites)
-   language settings

-   sections toggle to turn off/hide sections that will not be used
-   notifications
    -   add custom sounds to everything (ex: _ looking at notes _ "moan from Cumberbatch's Sherlock ")
    -   toggle what to notify about

### Sections

-   Tasks
    -   Have points
    -   Auto repeat
    -   schedulable
    -   needs confirmation after completion
    -   sound effects
-   achievements/milestones section to show progress on a specific act
    -   date and text of achievement
    -   picture
-   Habit tracker like functionality section to track things like drink water
-   achievements/milestones section to show progress on a specific act
-   diary/journal section
    -   customization (theme and background)
-   rewards and punishments (needs clarification)
-   mood tracker
    -   comment
    -   historical data
    -   check up
-   lists section (can have for example limits, rules, goals, toy inventory, wishlist)

-   maybe chat functionality
-   point system( needs clarification)
-   theme changer so it can appear kinky or like a normal vanilla app
-   local only
-   sections toggle to turn off/hide sections(features) that will not be used as not all will use everything and no one want a bloated up.
-   consequences: consequence of user doing or not doing a task (can be positive or negative or neutral)
    -   Rewards:
        -   can be Points base
        -   Dice option
    -   Punishments
        -   can be Points base
        -   Dice option
    -   Funishments
        -   can be Points base
        -   Dice option

### Tools

-   counter
-   stopwatch
-   countdown
-   clicker

### Other

-   Sound effects
-   visual customization
-   location tracking
-   Calender
    -   schedule things
-   chat
    -   customization
    -   media sharing
-   Kink list
    -   mutual quiz
