# Diary

## Overview

The Diary feature is a private space for users to document their experiences, thoughts, and reflections. It should offer both basic and advanced functionalities to cater to different user needs. The Diary should be secure, easy to use, and integrate seamlessly with other features of the app.

## Scope

In my opinion this feature should be independent from circles as users should not have their diaries deleted or lose access to it when leaving a circle.

## Minimum Elements

-   entry_content: the content of the diary for this entry
-   create_date: date/time that this entry has been created

## Core functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD diary (each action having its own permission)

#### Functionality

-   User can create a diary.
-   User can update an existing diary.
-   User can view/read a diary.
-   User can list/view all available diaries.
-   User can delete a diary.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on diaries.

#### Permissions list

-   **View Any diary**: User can view any (single) existing diary.
-   **View All diaries**: User can view all/list existing diaries.

#### Notes

The lack of create/update/delete permissions are intentional, as in my opinion no one other than the one who created the diary entry should have the ability edit or delete it.

The view all and view any permissions are for the rest of the users in the circle.

### 1. **CRUD**

-   Users should be able to [CRUD](../../extras/glossary.md#c) diary entries easily.

### 2. **Organize**

-   Ability to organize entries by date, tag, or custom folders.

### 3. **Access Control**

-   User (not another user or admin user) can choose who can see the diary entries and read them (like specific user or role or whole circle), but in my opinion user who created the entry should be the only one to be able to edit it or delete it.

## Optional functionalities

### Meta data support

The ability to set extra data on the diary entry, for example tags, this can help with filtering the entries.

### Support connecting to mood log functionality

The Ability to also attach a mood log entry with this functionality can also be a useful for the user.

## References

-   [Circle](../circle.md)
-   [Mood Log](mood_logs.md)
-   [Role](../cirlce_requirements/roles.md)
-   [Glossary](../../extras/glossary.md)
