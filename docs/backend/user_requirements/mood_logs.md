# Mood Log

## Overview

The Mood Log feature is a space for users to document their current feeling.

## Scope

In my opinion this feature should be independent from circles as users should not have their mood logs deleted or lose access to it when leaving a circle.

## Minimum Elements

-   emotions: list of emotions user currently feeling
-   create date: date/time that this entry has been created

## Core Functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD mood log (each action having its own permission)

#### Functionality

-   User can create a mood log.
-   User can update an existing mood log (their own only).
-   User can view/read a mood log.
-   User can list/view all available mood logs.
-   User can delete a mood log (their own only).

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on mood logs.

#### Permissions list

-   **View Any mood log**: User can view any (single) existing diary.
-   **View All mood logs**: User can view all/list existing diaries.

#### Notes

The lack of create/update/delete permissions are intentional, as in my opinion no one other than the one who created the mood log entry should have the ability edit or delete it.

The view all and view any permissions are for the rest of the users in the circle.

## Optional functionalities

### Emotions wheel

Some users might have trouble identifying their emotions so finding an emotional wheel (just google: emotions wheel) and using its hierarchy can help users with choosing what applies to them.

### Support connecting to diary functionality

The Ability to also attach a diary entry with this functionality can also be a useful for the user.

## References

-   [Circle](../circle.md)
-   [Diary](diaries.md)
-   [Role](../cirlce_requirements/roles.md)
-   [Glossary](../../extras/glossary.md)
