# List TBD

## Overview

Circle should have a lists that are shared between users in the circle.

## Requirements

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD lists (each action having its own permissions)

#### Functionality

-   User can create a list.
-   User can update an existing list.
-   User can view/read a list.
-   User can list/view all available lists.
-   User can delete a list.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on lists.

#### Permissions list

- **Create List**: User can create a list.
- **Update List**: User can update an existing list.
- **View Any List**: User can view any (single) existing list.
- **View All List**: User can view all/list existing lists.
- **Delete List**: User can delete an existing list.

## Optional functionalities

-   **Check list Support**

Support the ability to make the list a checklist, with also the possibility to uncheck/check all elements with a single action.

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)
-   [Glossary](../../extras/glossary.md)
