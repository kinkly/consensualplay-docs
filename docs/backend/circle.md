# Circle TBD

## Overview

A circle (other names: group, household, family, pen, etc...) is a core functionality of this app. A way to connect all participants who will be interacting together using the app.

The circle in my opinion should be the home for most of the app features except for diary and the mood log functionality.

## Core functionalities

### 1. **Joining and leaving a Circle**

Users should be able to join and leave circles with relative ease.

#### Optional functionalities

### 1. **Circle sharable id**

-   Circle can have a long easily readable id that can be shared with others who want to join the circle.

-   Circle should have the ability to reset its sharable ID incase it gets to others who are not wanted to be in.

### 2. **Circle join control**

-   Joining using easily readable circle id

-   Joining using invitation link

-   Circle can also have a joining password to again not allow users brute forcing their way into other circles

-   Users should be able to lock down a circle to disallow random users of joining, or having the ability to approve joining users

### 3. **Roles and permissions**

-   Circle admin (circle creator) can create and assign roles and permissions to all users within the circle to make it ork with any dynamic.

-   More in [Roles](cirlce_requirements/roles.md) and [Permissions](cirlce_requirements/permissions.md)

## References

-   [Roles](cirlce_requirements/roles.md)
-   [Permissions](cirlce_requirements/permissions.md)
-   [Glossary](../extras/glossary.md)
