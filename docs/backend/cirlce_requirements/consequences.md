# Consequences TBD

## Overview

Consequences, which can be positive or negative, can be assigned to users by other users or self or other automatic triggers.

## Requirements

-   [Circle](../circle.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **Actions**

#### Goal

The consequences can resulting actions, it can assign a task on a user and can be completed.

### 2. **Cost**

#### Goal

The consequences can have costs (user points), assigning them can effect users points (increasing/decreasing), also may be purchased using points.

### 3. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on consequences.

#### Permissions list

-   **Create consequence**: User can create a consequence.
-   **Update consequence**: User can update an existing consequence.
-   **Assign consequence to any**: User can assign an existing consequence to any (single) user.
-   **Assign consequence to all**: User can assign an existing consequence to all users.
-   **View Any consequence**: User can view any (single) existing consequence.
-   **View All consequences**: User can view all/list existing consequences.
-   **Delete consequence**: User can delete an existing consequence.

## References

-   [Circle](../circle.md)
-   [Points](points.md)
-   [Glossary](../../extras/glossary.md)
