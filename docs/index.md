# Consensual Play TBD

THIS PROJECT IS STILL IN THE "ALPHA" PHASE.

Welcome to **Consensual Play**, a collaborative project to create the most comprehensive kink play app wish list. This document is a living record of the features and requirements gathered from the community to build the perfect kink play experience.

## Source of ideas

All ideas [listed here](all_ideas.md) have been sourced from the community, particularly from discussions on Reddit. Where possible, I am including attributions and links to the original comments. If you notice any omissions or want to contribute additional details, feel free to suggest changes or provide further attributions.

## Open Source and Community-Driven

This document is **Open Source**, which means it is available for anyone who wishes to use it as a reference. Whether you’re looking to build a new app or integrate some of these features into an existing app, you’re welcome to draw from this resource. Contributions are encouraged and appreciated.

### Extra

-   [Attributions](attributions.md)
-   [Glossary](glossary.md)
-   [All Ideas List](all_ideas.md)
