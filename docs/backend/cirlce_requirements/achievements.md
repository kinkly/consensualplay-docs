# Achievement TBD

## Overview

In a circle an achievement is basically an entry to highlight a certain achievement one by user.

## Scope

This feature can be apart of a circle or a user, if its apart of a circle then it should be deleted after user leaves said circle, if it belongs to user then it should not be effected by leaving circle.

## Example

After one month of training, user can now do 20 pushups, they/other circle members want to highlight that, so they create a ne achievement entry.

    - target user: user who does the push ups
    - title: User can do 20 pushups
    - update_date: 01.02.2024

Later user was able to run a marathon, they want to highlight that.

    - target user: user who ran the marathon
    - title: User ran a 20km marathon.
    - update_date: 01.03.2024

## Core functionalities

### 1. **CRUD Actions**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can CRUD achievement (each action having its own permission)

#### Functionality

-   User can create a achievement.
-   User can update an existing achievement.
-   User can view/read a achievement.
-   User can list/view all available achievements.
-   User can delete a achievement.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on achievements.

#### Permissions list

-   **Create/Assign achievement to all**: User can create a achievement to all users.
-   **Update/Assign achievement to any**: User can update an existing achievement to any (single) users.
-   **View Any achievement**: User can view any (single) existing achievement.
-   **View All achievements**: User can view all/list existing achievements.
-   **Delete achievement**: User can delete an existing achievement.

## Optional functionalities

-   **Comment support**

Support the ability to add comments with achievements updates.

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Glossary](../../extras/glossary.md)
