# Points TBD

## Overview

Points are assigned to users in a circle, points can be manipulated either by automatic triggers or by other users. Points are represented as a number that can be 0 or positive or negative numbers (for simplicity numbers should be whole not fractions)

## Requirements

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)

## Scope

This functionality should be scoped in a circle.

## Core functionalities

### 1. **Update/Read Points**

#### Preconditions

-   User has sufficient roles or permissions

#### Goal

Users can give/take/view other users points

#### Functionality

-   User can give points to other user/users or them selfs.
-   User can take points from other user/users or them selfs.
-   User can view points of other user/users or them selfs.
-   User can view points transactions (points movements with dates) of other user/users or them selfs.

### 2. **Access Control Permissions**

#### Goal

This functionality should come with the following permissions to control what can users do, these permissions can be assigned to users to allow them to do actions on points.

#### Permissions list

-   **Update any points**: User can update a single users points.
-   **View Any points**: User can view any (current) users points.
-   **View All points**: User can view all/list users points transactions.

### 3. **Automatic updating**

#### Preconditions (any of the following)

-   [Tasks](tasks.md)
-   [Consequences](consequences.md)

#### Goal

Points should have the ability to automatically update (increase or decrease) depending on other triggers from the app: example user finishes an task successfully gets more points

#### Functionality

-   A trigger can update users points

## Optional functionalities

-   **Comment support**

Support the ability to add comments with points giving/taking, so one can see why points changed.

## References

-   [Circle](../circle.md)
-   [Roles](roles.md)
-   [Permissions](roles.md)
-   [Tasks](tasks.md)
-   [Consequences](consequences.md)
-   [Glossary](../../extras/glossary.md)
